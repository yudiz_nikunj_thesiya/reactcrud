export const initialState = {
	empData: [
		{
			id: 1,
			name: "Nik",
			age: 22,
			mobile: 7096490307,
			email: "nikunjthesiya.js@gmail.com",
			department: "ReactJS",
		},
		{
			id: 2,
			name: "Maulik",
			age: 28,
			mobile: 7898785858,
			email: "Maulik@gmail.com",
			department: "Laravel",
		},
		{
			id: 3,
			name: "Chirag",
			age: 27,
			mobile: 6898685645,
			email: "Chirag.js@gmail.com",
			department: "Android Development",
		},
		{
			id: 4,
			name: "Krushal",
			age: 20,
			mobile: 8758584525,
			email: "Krushal.js@gmail.com",
			department: "NodeJS",
		},
		{
			id: 5,
			name: "Rikesh Bhakta",
			age: 33,
			mobile: 9989858455,
			email: "rikesh@gmail.com",
			department: "ASP.NET",
		},
		{
			id: 6,
			name: "Pruthvik Patel",
			age: 30,
			mobile: 7875825354,
			email: "Pruthvik@gmail.com",
			department: "MERN",
		},
		{
			id: 7,
			name: "Vraj Patel",
			age: 19,
			mobile: 6358785455,
			email: "vraj@gmail.com",
			department: "PHP",
		},
	],
};

function reducer(state, action) {
	switch (action.type) {
		case "ADD":
			return {
				...state,
				empData: [...state.empData, { ...action.item }],
			};
		case "DELETE":
			let newEmpData = [...state.empData];
			const index = state.empData.findIndex(
				(empDataItem) => empDataItem.id === action.id
			);

			if (index >= 0) {
				newEmpData.splice(index, 1);
			}
			return { ...state, empData: newEmpData };
		case "UPDATE":
			const updatedData = state.empData.filter((empDataa) => {
				return empDataa.id != action.id;
			});

			return {
				...state,
				empData: [...updatedData, { ...action.item }],
			};

		default:
			return state;
	}
}

export default reducer;
