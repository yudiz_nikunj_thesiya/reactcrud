import React from "react";
import { Link } from "react-router-dom";

const Nav = () => {
	return (
		<div className="form__header">
			<Link to="/" className="form__header--logo">
				ReactCrud
			</Link>
			<Link to="/add" className="form__header--btn">
				+ Add Employee
			</Link>
		</div>
	);
};

export default Nav;
