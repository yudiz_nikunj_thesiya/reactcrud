import React, { useState } from "react";
import "../styles/pagination.scss";

const Pagination = ({ postPerPage, totalPosts, paginate }) => {
	const [currPage, setCurrPage] = useState(1);
	const pageNo = [];
	for (let i = 1; i <= Math.ceil(totalPosts / postPerPage); i++) {
		pageNo.push(i);
	}
	return (
		<div className="pagination" id="pagination">
			{pageNo.map((num) => (
				<li
					key={num}
					className={
						currPage === num ? "pagination__item active" : "pagination__item"
					}
					onClick={() => {
						paginate(num);
						setCurrPage(num);
					}}
				>
					<button className="pagination__link">{num}</button>
				</li>
			))}
		</div>
	);
};

export default Pagination;
