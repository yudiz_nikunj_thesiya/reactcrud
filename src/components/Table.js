import React, { useEffect, useState, useRef } from "react";
import "../styles/table.scss";
import { AiOutlineEdit } from "react-icons/ai";
import { MdDelete } from "react-icons/md";
import { FiSearch } from "react-icons/fi";
import { BsArrowUpShort, BsArrowDownShort } from "react-icons/bs";
import { Link } from "react-router-dom";
import { useStateValue } from "../state/StateProvider";
import Pagination from "./Pagination";

const Table = () => {
	const [{ empData }, dispatch] = useStateValue();
	const [empListData, setEmpListData] = useState(empData);
	const [currPage, setCurrPage] = useState(1);
	const [postPerPage, setPostPerPage] = useState(5);
	const [search, setSearch] = useState("");
	const [searchData, setSearchData] = useState();
	const [searchTerm, setSearchTerm] = useState("");
	const [asc, setAsc] = useState(false);
	const [desc, setDesc] = useState(false);

	const lastPost = currPage * postPerPage;
	const firstPost = lastPost - postPerPage;
	const currentPost = empListData.slice(firstPost, lastPost);

	const handleDelete = (id) => {
		// dispatch({
		// 	type: "DELETE",
		// 	id: id,
		// });

		const deleteEmp = empListData.filter((curr) => curr.id != id);
		setEmpListData(deleteEmp);
	};

	const paginate = (pageNo) => {
		setCurrPage(pageNo);
	};

	return (
		<div className="table">
			<form
				className="table__search"
				onSubmit={(e) => {
					e.preventDefault();
					setSearchTerm(search);
					setEmpListData();
				}}
			>
				<input
					type="text"
					className="table__search--input"
					placeholder="Search Employee"
					value={search}
					onChange={(e) => setSearch(e.target.value)}
				/>
				<button
					type="submit"
					className="table__search--btn"
					onClick={(e) => {
						e.preventDefault();
						setSearchTerm(search);
					}}
				>
					<FiSearch />
				</button>
			</form>
			<span className="table__heading">Employees Data</span>

			<table className="table__tbl">
				<thead>
					<tr>
						<th>Name</th>
						<th>
							<div className="age__sort">
								<span>Age</span>
								<span
									className="age__sort--asc"
									onClick={() => {
										setAsc(!asc);
										const sortAscData = empListData.sort(
											(a, b) => a.age - b.age
										);
										asc ? setEmpListData(sortAscData) : setEmpListData(empData);
									}}
								>
									<BsArrowUpShort />
								</span>
								<span
									className="age__sort--desc"
									onClick={() => {
										setDesc(!desc);
										const sortDescData = empListData.sort(
											(a, b) => b.age - a.age
										);
										asc
											? setEmpListData(sortDescData)
											: setEmpListData(empData);
									}}
								>
									<BsArrowDownShort />
								</span>
							</div>
						</th>
						<th>Mobile No.</th>
						<th>Email</th>
						<th>Department</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{searchTerm.trim().length > 0
						? empListData
								.filter((emp) => {
									if (
										emp.name.toLowerCase().includes(searchTerm.toLowerCase())
									) {
										return emp;
									}
								})
								.map((emp, index) => (
									<tr key={index}>
										<td>{emp.name}</td>
										<td>{emp.age}</td>
										<td>{emp.mobile}</td>
										<td>{emp.email}</td>
										<td>{emp.department}</td>
										<td>
											<div className="actions">
												<Link to={`/edit/${emp.id}`} className="btnEdit">
													<AiOutlineEdit />
												</Link>
												<button
													className="btnDelete"
													onClick={() => handleDelete(emp.id)}
												>
													<MdDelete />
												</button>
											</div>
										</td>
									</tr>
								))
						: currentPost?.map((emp, index) => (
								<tr key={index}>
									<td>{emp.name}</td>
									<td>{emp.age}</td>
									<td>{emp.mobile}</td>
									<td>{emp.email}</td>
									<td>{emp.department}</td>
									<td>
										<div className="actions">
											<Link to={`/edit/${emp.id}`} className="btnEdit">
												<AiOutlineEdit />
											</Link>
											<button
												className="btnDelete"
												onClick={() => handleDelete(emp.id)}
											>
												<MdDelete />
											</button>
										</div>
									</td>
								</tr>
						  ))}
				</tbody>
			</table>
			{searchTerm.trim().length <= 0 && (
				<Pagination
					postPerPage={postPerPage}
					totalPosts={empListData.length}
					paginate={paginate}
				/>
			)}
		</div>
	);
};

export default Table;
