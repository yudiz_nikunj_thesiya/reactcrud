import React, { useEffect, useRef, useState } from "react";
import "../styles/form.scss";
import { IoMdClose } from "react-icons/io";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useStateValue } from "../state/StateProvider";
import toast, { Toaster } from "react-hot-toast";
import { v4 as uuidv4 } from "uuid";

const Form = () => {
	const [{ empData }, dispatch] = useStateValue();
	// const [data, setData] = useState([]);
	const [emp, setEmp] = useState({});

	const inputName = useRef();
	const inputAge = useRef();
	const inputMobile = useRef();
	const inputEmail = useRef();
	const inputDepartment = useRef();

	let navigate = useNavigate();

	const handleSubmit = (e) => {
		e.preventDefault();

		if (
			inputName.current.value.trim().length === 0 ||
			inputAge.current.value.length === 0 ||
			inputMobile.current.value.length === 0 ||
			inputEmail.current.value.trim().length === 0 ||
			inputDepartment.current.value.trim().length === 0
		) {
			toast.error("Please Fill All The Fields!");
		} else {
			toast.success("Data Inserted Successfully!");
			dispatch({
				type: "ADD",
				item: {
					id: uuidv4(),
					name: inputName.current.value,
					age: inputAge.current.value,
					mobile: inputMobile.current.value,
					email: inputEmail.current.value,
					department: inputDepartment.current.value,
				},
			});
			navigate("/");
		}
	};

	return (
		<div className="form__container">
			<form className="form__main" onSubmit={() => handleSubmit}>
				<div className="form__main--header">
					<span className="heading">Add Details</span>
					<Link to="/" className="closeBtn">
						<IoMdClose />
					</Link>
				</div>
				<div className="form__main--inputs">
					<div className="input">
						<span>Name</span>
						<input
							type="text"
							className=""
							placeholder="Your Name"
							ref={inputName}
							required
						/>
					</div>
					<div className="input">
						<span>Age</span>
						<input
							type="number"
							className=""
							placeholder="Your Age"
							ref={inputAge}
							required
						/>
					</div>
					<div className="input">
						<span>Mobile No.</span>
						<input
							type="number"
							className=""
							placeholder="Mobile No."
							ref={inputMobile}
						/>
					</div>
					<div className="input">
						<span>Email</span>
						<input
							type="email"
							className=""
							placeholder="Email"
							ref={inputEmail}
							required
						/>
					</div>
					<div className="input">
						<span>Department</span>
						<input
							type="text"
							className=""
							placeholder="Your Department"
							ref={inputDepartment}
							required
						/>
					</div>
				</div>
				<div className="form__main--btns">
					<button type="submit" className="btn-submit" onClick={handleSubmit}>
						Add
					</button>
					<button type="reset" className="btn-reset">
						Cancel
					</button>
					<Toaster position="bottom-right" reverseOrder={true} />
				</div>
			</form>
		</div>
	);
};

export default Form;
