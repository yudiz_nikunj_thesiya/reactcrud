import React, { useEffect, useRef, useState } from "react";
import "../styles/form.scss";
import { IoMdClose } from "react-icons/io";
import { Link, useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useStateValue } from "../state/StateProvider";
import toast, { Toaster } from "react-hot-toast";

import { v4 as uuidv4 } from "uuid";

const EditForm = () => {
	// const [data, setData] = useState([]);

	const [emp, setEmp] = useState({});
	const [editData, setEditData] = useState();

	const { id } = useParams();
	const [{ empData }, dispatch] = useStateValue();

	useEffect(() => {
		const fetchEmp = empData.filter((curr) => curr.id == id);
		// console.log(fetchEmp[0]);
		setEditData(fetchEmp[0]);
	}, []);

	const [inputName, setInputName] = useState(editData?.name);
	const [inputAge, setInputAge] = useState(editData?.age);
	const [inputMobile, setInputMobile] = useState(editData?.mobile);
	const [inputEmail, setInputEmail] = useState(editData?.email);
	const [inputDepartment, setInputDepartment] = useState(editData?.department);

	let navigate = useNavigate();

	const handleUpdate = (eid) => {
		if (
			inputName?.length === 0 ||
			inputAge?.length === 0 ||
			inputMobile?.length === 0 ||
			inputEmail?.length === 0 ||
			inputDepartment?.length === 0
		) {
			toast.error("Please Fill All The Fields!");
		} else {
			toast.success("Data Updated Successfully!");
			dispatch({
				type: "UPDATE",
				id: eid,
				item: {
					id: uuidv4(),
					name: inputName,
					age: inputAge,
					mobile: inputMobile,
					email: inputEmail,
					department: inputDepartment,
				},
			});
			navigate("/");
		}
	};

	return (
		<div className="form__container">
			<form
				className="form__main"
				onSubmit={(e) => {
					e.preventDefault();
					handleUpdate(id);
				}}
			>
				<div className="form__main--header">
					<span className="heading">Add Details</span>
					<Link to="/" className="closeBtn">
						<IoMdClose />
					</Link>
				</div>
				<div className="form__main--inputs">
					<div className="input">
						<span>Name</span>
						<input
							type="text"
							className=""
							placeholder={editData?.name}
							value={inputName}
							onChange={(e) => setInputName(e.target.value)}
							required
						/>
					</div>
					<div className="input">
						<span>Age</span>
						<input
							type="number"
							className=""
							placeholder={editData?.age}
							value={inputAge}
							onChange={(e) => setInputAge(e.target.value)}
							required
						/>
					</div>
					<div className="input">
						<span>Mobile No.</span>
						<input
							type="number"
							className=""
							placeholder={editData?.mobile}
							value={inputMobile}
							onChange={(e) => setInputMobile(e.target.value)}
							required
						/>
					</div>
					<div className="input">
						<span>Email</span>
						<input
							type="email"
							className=""
							placeholder={editData?.email}
							value={inputEmail}
							onChange={(e) => setInputEmail(e.target.value)}
							required
						/>
					</div>
					<div className="input">
						<span>Department</span>
						<input
							type="text"
							className=""
							placeholder={editData?.department}
							value={inputDepartment}
							onChange={(e) => setInputDepartment(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="form__main--btns">
					<button type="submit" className="btn-submit">
						Update
					</button>
					<Link to="/" className="btn-reset">
						Cancel
					</Link>
					<Toaster position="bottom-right" reverseOrder={true} />
				</div>
			</form>
		</div>
	);
};

export default EditForm;
