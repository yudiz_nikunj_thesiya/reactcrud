import React, { useState } from "react";
import Form from "./components/Form";
import "./styles/form.scss";
import "./styles/app.scss";
import { Routes, Route } from "react-router-dom";
import Nav from "./components/Nav";
import Table from "./components/Table";
import EditForm from "./components/EditForm";

function App() {
	const [form, setForm] = useState(false);

	return (
		<div className="form">
			<Nav />
			<Routes>
				<Route path="/" element={<Table />} />
				<Route path="/add" element={<Form />} />
				<Route path="/edit/:id" element={<EditForm />} />
			</Routes>
		</div>
	);
}

export default App;
